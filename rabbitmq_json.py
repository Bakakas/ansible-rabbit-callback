from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

DOCUMENTATION = '''
'''

import json
import pika
import uuid
from ansible.plugins.callback import CallbackBase


class CallbackModule(CallbackBase):
    """
    This callback module send ansible out to rabbitmq with json format
    """
    CALLBACK_VERSION = 2.0
    CALLBACK_TYPE = 'aggregate'
    CALLBACK_NAME = 'rabbitmq_json'

    CALLBACK_NEEDS_WHITELIST = True

    def __init__(self, purge=False):

        super(CallbackModule, self).__init__()
        
        #Парметры коннекта к rabbitmq
        self.rabbit_host = '127.0.0.1'
        self.rabbit_key = 'ansible'
        self.rabbit_queue = 'ansible'
        self.rabbit_exchange = ''
        self.rabbit_exchange_type = 'fanout'
        


        # Инициализация коннекта
        self.connection = pika.BlockingConnection(pika.ConnectionParameters(host=self.rabbit_host,heartbeat_interval=0))
        self.channel = self.connection.channel()
        # Создаем очередь
        self.channel.queue_declare(queue=self.rabbit_queue, durable=True)
        if purge:
            self.channel.queue_purge(queue=self.rabbit_queue)


    def send(self, msg):
        # Побликуем сообщение
        self.channel.basic_publish(exchange=self.rabbit_exchange,
                      routing_key=self.rabbit_key,
                      body=json.dumps(msg, indent=2),
                      properties=pika.BasicProperties(content_type='application/json', delivery_mode=2))

    #Переопределяем методы callback
    def runner_on_failed(self, host, res, ignore_errors=False):
        self.send({'id': str(uuid.uuid4()), 'host': host, 'status': 'FAILED', 'msg': self._dump_results(res)})
    
    def runner_on_ok(self, host, res):
        self.send({'id': str(uuid.uuid4()), 'host': host, 'status': 'OK', 'msg': self._dump_results(res)})

    def runner_on_skipped(self, host, item=None):
        self.send({'id': str(uuid.uuid4()), 'host': host, 'status': 'SKIPPED', 'msg': 'SKIPPED'})
    
    def runner_on_unreachable(self, host, res):
        self.send({'id': str(uuid.uuid4()), 'host': host, 'status': 'UNREACHABLE', 'msg': self._dump_results(res)})
    
    def runner_on_async_failed(self, host, res, jid):
        self.send({'id': str(uuid.uuid4()), 'host': host, 'status': 'FAILED', 'msg': self._dump_results(res)})
    
    def playbook_on_import_for_host(self, host, imported_file):
        self.send({'id': str(uuid.uuid4()), 'host': host, 'status': 'imported file', 'msg': imported_file})

    def playbook_on_not_import_for_host(self, host, missing_file):
        self.send({'id': str(uuid.uuid4()), 'host': host, 'status': 'missing file', 'msg': missing_file})
